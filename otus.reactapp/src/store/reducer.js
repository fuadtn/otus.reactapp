const ADD_USER = 'ADD_USER';
const GET_USER = 'GET_USER';

const defaultState = {
    users: [
        {
            email: 'root@otus',
            password: '12345'
        },
        {
            email: 'guest@otus',
            password: ''
        }
    ]
}

export const reducer = (state = defaultState, action) => {
    switch (action.type) {
        case ADD_USER:
            return { ...state, users: [...state.users, action.payload] }
        //case GET_USER:
        //    return {...}
        default:
            return state;
    }
}
