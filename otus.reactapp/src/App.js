import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";

import {
    Route,
    Routes,
    BrowserRouter
} from "react-router-dom";

import SignInForm from './components/SignInForm';
import Screen from './components/Screen';
import RegisterForm from './components/RegisterForm';
import { useSelector } from 'react-redux';
import Header from './components/Header';


function App() {
    const users = useSelector(state => state.users);

    return (
        <BrowserRouter>
            <Header/>
            <Routes>
                <Route path="/" element={
                    <Screen>
                        <h1>USERS</h1>
                        <br/>
                        {
                            users.map((user) =>
                            (<div key={Math.random()}>
                                <a>{user['email']}</a>
                                <br/>
                            </div>)
                            )
                        }
                    </Screen>
                } />
                <Route path="/signin" element={<SignInForm/>} />
                <Route path="/register" element={<RegisterForm/>} />
                <Route path="*" element={<Screen color="darkred">NOT FOUND</Screen>} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;
