import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Screen from './Screen';


export default function SignInForm(props) {
    const [resultForm, setResultForm] = useState(null);
    const users = useSelector(state => state.users);

    function SignIn(e) {
        const data = new FormData(e.target);
        let currentUser = {
            email: data.get('email'),
            password: data.get('password')
        }

        for (const user of users)
        {
            if (JSON.stringify(user) === JSON.stringify(currentUser))
                return setResultForm(<Screen color="green">SUCCESS</Screen>);
        }
        return setResultForm(<Screen color="darkred">FAILED</Screen>);
    }

    return (
        resultForm
            ? resultForm
            :
            <div className="sign-in-form-container">
                    <form className="sign-in-form" onSubmit={SignIn}>
                        <div className="sign-in-form-content">
                            <h3 className="sign-in-form-title">Sign In</h3>

                            <div className="form-group mt-3">
                                <input
                                    type="email" 
                                    name="email"
                                    className="form-control mt-1"
                                    placeholder="Enter email"
                                />
                            </div>
                            <div className="form-group mt-3">
                                <input
                                    type="password" 
                                    name="password" 
                                    className="form-control mt-1"
                                    placeholder="Enter password"
                                />
                            </div>
                            <div className="d-grid gap-2 mt-3">
                                <button type="submit" className="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                            <label className="forgot-password text-right mt-2">
                                login: root@otus
                                <br/>
                                password: 12345
                            </label>
                        </div>
                    </form>
                </div>
    );
}
