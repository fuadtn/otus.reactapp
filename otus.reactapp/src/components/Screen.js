import React from 'react';
import PropTypes from 'prop-types';


export default function Screen(prop) {

    var colorStyle = {
        color: prop.color,
    }

    return (
        <div className="screen" style={colorStyle}><div className="screen-container">{prop.children}</div></div>
    );
}

Screen.propTypes = {
    color: PropTypes.string
}