import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Screen from './Screen';


export default function RegisterForm() {
    const [resultForm, setResultForm] = useState(null);    
    const users = useSelector(state => state.users);
    const dispatch = useDispatch();

    function Register(e) {
        e.preventDefault();
        const data = new FormData(e.target);
        let currentUser = {
            email: data.get('email'),
            password: data.get('password')
        };

        for (const user of users) {
            if (user['email'] == currentUser['email'])
                return setResultForm(<Screen color="darkred">FAILED. Email is already in use</Screen>);
        }

        dispatch({ type: 'ADD_USER', payload: currentUser });
        return setResultForm(<Screen color="green">SUCCCESS. User {currentUser['email']} added</Screen>);
    }

    return (
        resultForm
            ? resultForm
            :
                <div className="sign-in-form-container">
                    <form className="sign-in-form" onSubmit={Register}>
                        <div className="sign-in-form-content">
                            <h3 className="sign-in-form-title">Register</h3>

                            <div className="form-group mt-3">
                                <input
                                    type="email" 
                                    name="email"
                                    className="form-control mt-1"
                                    placeholder="Enter email"
                                />
                            </div>
                            <div className="form-group mt-3">
                                <input 
                                    type="password" 
                                    name="password" 
                                    className="form-control mt-1"
                                    placeholder="Enter password"
                                />
                            </div>
                            <div className="d-grid gap-2 mt-3">
                                <button type="submit" className="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
    );
}
