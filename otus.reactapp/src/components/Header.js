import { NavLink } from "react-router-dom";


export default function Header() {
    return (
        <header className="menu">
            <NavLink to={'/'} className="menu-link">Home</NavLink>
            <NavLink to={'/signin'} className="menu-link">Sign In</NavLink>
            <NavLink to={'/register'} className="menu-link">Register</NavLink>
        </header>
    );
}
